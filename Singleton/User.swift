//
//  User.swift
//  Singleton
//
//  Created by Rafiul Hasan on 16/01/2019.
//  Copyright © 2019 Rafiul Hasan. All rights reserved.
//

import Foundation

struct User: Codable {
	let name: String
	let email: String
	let bio: String
}
